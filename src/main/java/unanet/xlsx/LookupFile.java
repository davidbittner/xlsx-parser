package unanet.xlsx;

import com.opencsv.CSVReader;

import java.util.ArrayList;
import java.io.*;

class LookupFile {
    static final int ID_ROW       = 0;
    static final int ORG_CODE_ROW = 1;
    static final int PRO_CODE_ROW = 2;

    ArrayList<String[]> data;
    String []headers;

    CSVReader reader;

    LookupFile(String path) throws FileNotFoundException, IOException {
        reader = new CSVReader(new FileReader(path));

        String []currentRow = reader.readNext();
        headers = currentRow;
        
        while((currentRow = reader.readNext()) != null) {
            data.add(currentRow);
        }
    }

    //Could sort and use a binary search to find items
    //But, unecessary for the size of the files.
    //This is good enough
    String getProjectCode(String id) {
        for(String[] row : data) {
            if(row[ID_ROW].equals(id)) {
                return row[PRO_CODE_ROW];
            }
        }

        return null;
    }

    String getProjectOrgCode(String id) {
        for(String[] row : data) {
            if(row[ID_ROW].equals(id)) {
                return row[ORG_CODE_ROW];
            }
        }

        return null;
    }
}
