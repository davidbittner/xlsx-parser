package unanet.xlsx;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;

import java.util.ArrayList;

abstract class RowHandler {
    ArrayList<Row> buffer;

    final LookupFile lookerupper;

    RowHandler(LookupFile looker) {
        lookerupper = looker;

        buffer = new ArrayList<>();
    }
    
    abstract boolean predicate(Row file_row);
    abstract void    giveRow(Row file_row);

    abstract ReturnInformation processBuffer();
    abstract String getName();

    protected String getCell(Row row, int index) throws IndexOutOfBoundsException {
        Cell check_cell = row.getCell(index, MissingCellPolicy.RETURN_BLANK_AS_NULL);

        if(check_cell == null) {
            return "";
        }else{
            DataFormatter formatter = new DataFormatter();
            if(check_cell.getCellType() == CellType.FORMULA) {
                try{
                    CellValue val = XlsxParser.getEval().evaluate(check_cell);
                    return val.formatAsString();
                }catch(IndexOutOfBoundsException ex) {
                    return "";
                }
            }else{
                return formatter.formatCellValue(check_cell);
            }
        }
    }
}
