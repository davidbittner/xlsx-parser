package unanet.xlsx;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;

public class LaborRow extends RowHandler {
    static final int WIDTH = 10;

    static final int UNIT_OF_MEASURE_COL = 9;
    static final int USERNAME_COL        = 3;
    static final int ORG_CODE_COL        = 0;
    static final int PROJECT_CODE_COL    = 1;
    static final int TASK_NAME_COL       = 2;
    static final int HOURS_COL           = 8;
    static final int COST_RATE_COL       = 10;
    static final int LABOR_CATEGORY_COL  = 5;
    static final int WORK_DATE_COL       = 7;
    static final int COMMENTS_COL        = 6;

    static final String []HEADERS = {
        "*Username",
        "Work_Date",
        "Project_Org_Code",
        "Project_Code",
        "Task_Name",
        "Pay_Code",
        "Hours",
        "Cost_Rate",
        "Labor_Category",
        "Comments"
    };

    LaborRow(LookupFile looker) {
        super(looker);
    }

    String getName() {
        return "labor";
    }

    boolean predicate(Row file_row) {
        return getCell(file_row, UNIT_OF_MEASURE_COL).toLowerCase().equals("hour");
    }

    void giveRow(Row file_row) {
        if(predicate(file_row)) {
            buffer.add(file_row);
        }
    }

    ReturnInformation processBuffer() {
        ReturnInformation ret = new ReturnInformation();

        ArrayList<String[]> ret_array = new ArrayList<>();

        for(Row row : buffer) {
            String []data = new String[WIDTH];

            data[0] = getCell(row, USERNAME_COL);
            data[1] = getCell(row, WORK_DATE_COL);
            data[2] = getCell(row, ORG_CODE_COL);
            data[3] = getCell(row, PROJECT_CODE_COL);
            data[4] = getCell(row, TASK_NAME_COL);
            data[5] = "DS";
            data[6] = getCell(row, HOURS_COL);
            data[7] = getCell(row, COST_RATE_COL);
            data[8] = getCell(row, LABOR_CATEGORY_COL);
            data[9] = getCell(row, COMMENTS_COL);

            ret_array.add(data);
        }

        ret.headers = LaborRow.HEADERS;
        ret.data = ret_array;

        return ret;
    }
}
