package unanet.xlsx;

import java.util.ArrayList;

class ReturnInformation {
    public String[] headers;
    public ArrayList<String[]> data;
}
